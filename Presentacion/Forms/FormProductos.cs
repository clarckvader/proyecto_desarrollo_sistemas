﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion.Forms
{
    public partial class FormProductos : Form
    {
        private CN_Clientes objcli = new CN_Clientes();
        public FormProductos()
        {
            InitializeComponent();
        }

        private void FormProductos_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = objcli.Productos();
        }
    }
}
