﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
namespace Presentacion.Forms
{
    public partial class Ventas : Form
    {
        private CN_Clientes objCli = new CN_Clientes();
        public Ventas()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Ventas_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = objCli.MostrarVentas();
        }
    }
}
