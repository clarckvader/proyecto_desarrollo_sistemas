﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
namespace Presentacion.Forms
{
    public partial class Clientes : Form
    {
        private CN_Clientes objCli = new CN_Clientes();
        private string idCliente = null;
        private bool Editar = false;
        
        public Clientes()
        {
            InitializeComponent();
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            MostrarClientes();
            LoadDataToTextBox();
            setBtnGuardarState(false);
            setTextBoxState(false);
        }

        private void setTextBoxState(bool isEnable)
        {
            txtNombre.Enabled = isEnable;
            txtApellido.Enabled = isEnable;
            txtDireccion.Enabled = isEnable;
            txtTelefono.Enabled = isEnable;
        }

        private void setBtnGuardarState(bool isEnable)
        {
            btnGuardar.Enabled = isEnable;
        }

        private void LoadDataToTextBox()
        {
            Editar = true;
            txtNombre.Text = dgvClientes.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellido.Text = dgvClientes.CurrentRow.Cells["Apellido"].Value.ToString();
            txtDireccion.Text = dgvClientes.CurrentRow.Cells["Direccion"].Value.ToString();
            txtTelefono.Text = dgvClientes.CurrentRow.Cells["Telefono"].Value.ToString();
            idCliente = dgvClientes.CurrentRow.Cells["idClient"].Value.ToString();
        }

        private void MostrarClientes()
        {
            CN_Clientes obj = new CN_Clientes();
            dgvClientes.DataSource = obj.MostrarClient();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            setTextBoxState(true);
            Editar = false;
            MostrarClientes();
            cleanTextBox();
            setBtnGuardarState(true);

            label4.Text = "Crear nueva materia";
            btnGuardar.Text = "Guardar";
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvClientes.SelectedCells.Count > 0)
            {
                LoadDataToTextBox();
                setTextBoxState(false);
                btnGuardar.Text = "Guardar";
                setBtnGuardarState(false);
            }
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            setTextBoxState(true);
            LoadDataToTextBox();
            MostrarClientes();
            setBtnGuardarState(true);

            btnGuardar.Text = "Guardar Cambios";
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(dgvMaterias.SelectedRows.Count);
            if (dgvClientes.SelectedRows.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Deseas eliminar esta materia?",
                      "Confirmacion", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        idCliente = dgvClientes.CurrentRow.Cells["Id"].Value.ToString();
                        objCli.eliminarCliente(idCliente);
                        MessageBox.Show("Cliente eliminada correctamente");
                        MostrarClientes();
                        setTextBoxState(false);
                        break;
                    case DialogResult.No:
                        break;
                }

                cleanTextBox();
            }
            else
            {
                MessageBox.Show("Porfavor seleccione una fila");
            }
        }

        private void cleanTextBox()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtDireccion.Clear();
            txtTelefono.Clear();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (txtBuscar.Text == "")
            {
                MessageBox.Show("Escriba un termino de busqueda");
            }
            else
            {
                CN_Clientes obj = new CN_Clientes();
                dgvClientes.DataSource = obj.BuscarCli(txtBuscar.Text);
                setTextBoxState(false);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" && txtApellido.Text != "" && txtDireccion.Text != "" && txtTelefono.Text != "")
            {
                if (Editar == false)
                {
                    try
                    {
                        objCli.insertarClientes(txtNombre.Text, txtApellido.Text, txtDireccion.Text, txtTelefono.Text);
                        MessageBox.Show("Se inserto correctamente");
                        MostrarClientes();
                        setTextBoxState(false);
                        cleanTextBox();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Ocurrio un error:" + ex);
                    }

                }
                else if (Editar == true)
                {
                    try
                    {
                        objCli.editarCliente(txtNombre.Text, txtApellido.Text, txtDireccion.Text, txtTelefono.Text, idCliente);
                        MessageBox.Show("Se edito correctamente");
                        MostrarClientes();
                        setTextBoxState(false);
                        cleanTextBox();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Ocurrio un error:" + ex);
                    }

                }

                //label4.Text = "Seleccione una accion";
                btnGuardar.Text = "Guardar";
                setBtnGuardarState(false);

            }
            else
            {
                MessageBox.Show("No puede dejar ningun campo vacio");
            }
        }
    }
}
