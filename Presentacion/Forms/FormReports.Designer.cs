﻿namespace Presentacion.Forms
{
    partial class FormReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.farmacia2DataSet1 = new Presentacion.farmacia2DataSet1();
            this.farmacia2DataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VentasTotalesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ClienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.farmacia2DataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.farmacia2DataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VentasTotalesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClienteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "FarmaDS";
            reportDataSource1.Value = this.VentasTotalesBindingSource;
            reportDataSource2.Name = "DataSet1";
            reportDataSource2.Value = this.ClienteBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Presentacion.Productos.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // farmacia2DataSet1
            // 
            this.farmacia2DataSet1.DataSetName = "farmacia2DataSet1";
            this.farmacia2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // farmacia2DataSet1BindingSource
            // 
            this.farmacia2DataSet1BindingSource.DataSource = this.farmacia2DataSet1;
            this.farmacia2DataSet1BindingSource.Position = 0;
            // 
            // VentasTotalesBindingSource
            // 
            this.VentasTotalesBindingSource.DataMember = "VentasTotales";
            this.VentasTotalesBindingSource.DataSource = this.farmacia2DataSet1;
            // 
            // ClienteBindingSource
            // 
            this.ClienteBindingSource.DataMember = "Cliente";
            this.ClienteBindingSource.DataSource = this.farmacia2DataSet1;
            // 
            // FormReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "FormReports";
            this.Text = "FormReports";
            this.Load += new System.EventHandler(this.FormReports_Load);
            ((System.ComponentModel.ISupportInitialize)(this.farmacia2DataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.farmacia2DataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VentasTotalesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClienteBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource farmacia2DataSet1BindingSource;
        private farmacia2DataSet1 farmacia2DataSet1;
        private System.Windows.Forms.BindingSource VentasTotalesBindingSource;
        private System.Windows.Forms.BindingSource ClienteBindingSource;
    }
}