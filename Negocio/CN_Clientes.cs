﻿using Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class CN_Clientes
    {
        private CD_Clientes objCD = new CD_Clientes();

        public DataTable MostrarClient()
        {
            DataTable table = new DataTable();
            table = objCD.Mostrar();
            return table;

        }
        public DataTable MostrarVentas()
        {
            DataTable table = new DataTable();
            table = objCD.Ventas();
            return table;

        }

        public DataTable Productos() {
            DataTable table = new DataTable();
            table = objCD.Ventas();
            return table;

        }
        public DataTable BuscarCli(string searchTerm)
        {
            DataTable table = new DataTable();
            table = objCD.Buscar(searchTerm);
            return table;

        }

        public void insertarClientes(string Nombre, string Apellido, string Direccion, string Telefono)
        {

            objCD.CrearCliente(Nombre, Apellido, Direccion, Telefono);

        }

        public void editarCliente(string Nombre, string Apellido, string Direccion, string Telefono, string id)
        {
            objCD.Editar(Nombre, Apellido, Direccion, Telefono, Convert.ToInt32(id));
        }

        public void eliminarCliente(string id)
        {
            objCD.Eliminar(Convert.ToInt32(id));
        }
    }
}
