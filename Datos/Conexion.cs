﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    class Conexion
    {
        private SqlConnection Conn = new SqlConnection("Server=PC;Database=farmacia2;User Id=sa;Password=manuel955;");

        public SqlConnection AbrirConexion()
        {
            if (Conn.State == System.Data.ConnectionState.Closed)
                Conn.Open();
            return Conn;

        }

        public SqlConnection CerrarConexion()
        {

            if (Conn.State == System.Data.ConnectionState.Open)
                Conn.Close();
            return Conn;

        }
    }
}
